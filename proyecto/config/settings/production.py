from .base import *
from os.path import join
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from multifiberpy.sentry_multifiber import send_slack_event

from pathlib import Path
from dotenv import load_dotenv
from . import get_env_variable

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)


DEBUG = True

ENVIROMENT = 'PRO'

sentry_sdk.init(
    dsn=get_env_variable('DSN'),
    integrations=[DjangoIntegration()],
    before_send=send_slack_event
)



SECRET_KEY = get_env_variable('SECRET_KEY')

# CELERY_BROKER_URL = 'redis://localhost:6379'
# CELERY_RESULT_BACKEND = CELERY_BROKER_URL
# CELERY_BROKER_TRANSPORT = 'redis'


ALLOWED_HOSTS = ['*']

STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    join(BASE_DIR, 'staticfiles'),
)


BASE_DIR_LOG = join(dirname(BASE_DIR), 'logs')

LOGGING = {
    "version": 1,
    "formatters": {"verbose": {"format": "%(levelname)s:%(name)s: %(message)s"}},
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": join(BASE_DIR_LOG, "debug_django.log"),
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "*":{
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": False,        
        }
    },
}

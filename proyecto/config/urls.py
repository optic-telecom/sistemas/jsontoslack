from django.contrib import admin
from utils.views import SwaggerSchemaView, home
from django.urls import path, re_path, include
from users.urls import urlpatterns as urlpatterns_user
from users.urls import router as router_users

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_users.urls)),
    path('api/docs', SwaggerSchemaView.as_view()),
] + urlpatterns_user

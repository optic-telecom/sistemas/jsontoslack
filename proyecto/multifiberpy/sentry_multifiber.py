import json
import urllib3 
from django.conf import settings


def send_slack_event(event, hint):
    http = urllib3.PoolManager()
    fields = []

    fields.append({
        "title": "Función",
        "value": event['exception']['values'][-1]['stacktrace']['frames'][-1]['function'],
        "short": True
    })
    fields.append({
        "title": "Tipo",
        "value": event['exception']['values'][-1]['type'],
        "short": True
    })

    fields.append({
        "title": "Valor",
        "value": event['exception']['values'][-1]['value'],
        "short": True
    })

    fields.append({
        "title": "Archivo",
        "value": event['exception']['values'][-1]['stacktrace']['frames'][-1]['abs_path'] + ':' + str(event['exception']['values'][-1]['stacktrace']['frames'][-1]['lineno']),
        "short": False
    })

    proyecto = 'Proyecto: ' + settings.SITE_NAME
    if hasattr(settings.ENVIROMENT):
        proyecto = proyecto + ' - ' + settings.ENVIROMENT
        
    data_slack = {
        "text": proyecto,
        "attachments": [
          {
            "color": "#c82d10",
            "text": event['level'],
            "fields": fields
          }       
        ]
    }


    response_slack = http.request(
        "POST",
        str('https://hooks.slack.com/services/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'),
        body=json.dumps(data_slack),
        headers={'Content-Type': 'application/json'}
    )
    return event
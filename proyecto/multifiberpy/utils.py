
def timedelta_format(td):
    if td.days > 365:
        year = td.days//365
        days = td.days - (year * 365)
        result = year, days , td.seconds//3600, (td.seconds//60)%60, td.seconds%60
        return "{}a {}d {}h {}m {}s".format(*result)
    else:
        result = td.days, td.seconds//3600, (td.seconds//60)%60, td.seconds%60
        return "{}d {}h {}m {}s".format(*result)
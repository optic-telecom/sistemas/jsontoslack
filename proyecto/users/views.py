from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.generics import RetrieveAPIView, UpdateAPIView, CreateAPIView, get_object_or_404
from rest_framework.views import APIView
from .serializers import UserSerializer, CreateUserSerializer,PasswordResetSerializer
from utils.views import send_webhook_transaction
from utils.permissions import IsSystemInternalPermission

User = get_user_model()

class UserViewSet(viewsets.ModelViewSet):
    """
    create:
        Create a user
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def get_serializer_class(self):
        if self.request.method.lower() == "post":
            return CreateUserSerializer
        return super(UserViewSet, self).get_serializer_class()

    def get_permissions(self):
        if self.request.method.lower() == "post":
            return [AllowAny()]
        return super(UserViewSet, self).get_permissions()

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='user', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    
    def destroy(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        instande_id = instance.id
        self.perform_destroy(instance)
        if not replicate: # Si tiene replicate ya se replicó
            data = {
                'id': str(instande_id)
            }
            send_webhook_transaction(model='user', action=3, data=data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='user', action=2, data=serializer.data)

        return Response(serializer.data)


class UserView(RetrieveAPIView, UpdateAPIView):
    """
    get:
        Connected user information

    put:
        Update user information.

    pacth:
        Update user information.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsSystemInternalPermission|IsAuthenticated,)

    def get_object(self):
        return self.request.user

    def update(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        partial = kwargs.pop('partial', False)
        replicate = request.data.get('replicate', None)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='user', action=2, data=serializer.data)

        return Response(serializer.data)


class PasswordReset(CreateAPIView):
    """
    post:
        End point for password change

    """
    serializer_class = PasswordResetSerializer

    def create(self, request, *args, **kwargs):
        """ Envía una transacción al endpoint de WebHookTransaction si no se ha replicado aún """
        replicate = request.data.get('replicate', None)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        if not replicate: # Si tiene replicate ya se replicó
            send_webhook_transaction(model='user', action=1, data=serializer.data)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)